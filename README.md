# Installation
Before `ROI.plugin.gurobi` can be installed `gurobi` and its corresponding 
**R** package need to be installed. Both can be obtained from
[https://www.gurobi.com](https://www.gurobi.com) 
and more information about the installation can be found in the `Quick Start Guides` at
[https://www.gurobi.com/documentation](https://www.gurobi.com/documentation).    


`ROI.plugin.gurobi` was tested on `Debian GNU/Linux 11 (bullseye)` with the
`gurobi` version `10`.   


After `gurobi` and `gurobi` (**R** package) are installed simply use
```r
remotes:::install_gitlab("roigrp/solver/ROI.plugin.gurobi", INSTALL_opts = "--no-multiarch")
```
to install `ROI.plugin.gurobi`.

## Control Parameters
An overview on all the available Gurobi parameters can be found at
[`https://www.gurobi.com/documentation/10.0/refman/parameters.html`](https://www.gurobi.com/documentation/10.0/refman/parameters.html).

## Usage
Several usage examples can be found in the **vignettes** folder.
