PKG=ROI.plugin.gurobi

readme:
	R -e 'rmarkdown::render("README.Rmd", clean = FALSE)'
		
build:
	R CMD build .

inst: build
	R CMD INSTALL ${PKG}*.tar.gz
	
check: build
	R CMD check ${PKG}*.tar.gz

manual: clean
	R CMD Rd2pdf --output=Manual.pdf .

clean:
	rm -f Manual.pdf README.knit.md README.html
	rm -rf .Rd2pdf*
